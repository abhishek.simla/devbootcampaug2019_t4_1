import org.junit.Assert;
import org.junit.Test;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

public class GameTest {

    @Test
    public void shouldGetScoreAfterRound1() {
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        PrintStream console = mock(PrintStream.class);
        when(player1.getBehaviour()).thenReturn(mock(CooperateBehaviour.class) );
        when(player2.getBehaviour()).thenReturn(mock(CooperateBehaviour.class) );
        when(player1.move()).thenReturn(Move.COPERATE);
        when(player2.move()).thenReturn(Move.COPERATE);
        when(player1.score()).thenReturn(2);
        when(player2.score()).thenReturn(2);
        Score score = new Score(2, 2);
        when(machine.calculateScore(Move.COPERATE, Move.COPERATE)).thenReturn(score);
        Game game = new Game(player1, player2, machine, 1, console);

        game.play();

        verify(player1).move();
        verify(player2).move();
        verify(machine).calculateScore(Move.COPERATE, Move.COPERATE);
        verify(player1).upateScore(2);
        verify(player2).upateScore(2);
        // test console
        verify(console).println("Player 1 : 2");
        verify(console).println("Player 2 : 2");
    }

    @Test
    public void shouldGetScoreAfterRound2() {
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        PrintStream console = mock(PrintStream.class);
        when(player1.getBehaviour()).thenReturn(mock(CooperateBehaviour.class) );
        when(player2.getBehaviour()).thenReturn(mock(CooperateBehaviour.class) );
        when(player1.move()).thenReturn(Move.COPERATE);
        when(player2.move()).thenReturn(Move.COPERATE);
        when(player1.score()).thenReturn(2, 4);
        when(player2.score()).thenReturn(2, 4);
        Score score1 = new Score(2, 2);
        Score score2 = new Score(4, 4);
        when(machine.calculateScore(Move.COPERATE, Move.COPERATE)).thenReturn(score1, score2);
        Game game = new Game(player1, player2, machine, 2, console);

        game.play();

        verify(player1, times(2)).move();
        verify(player2, times(2)).move();
        verify(machine, times(2)).calculateScore(Move.COPERATE, Move.COPERATE);
        verify(player1).upateScore(2);
        verify(player2).upateScore(2);
        verify(player1).upateScore(4);
        verify(player2).upateScore(4);
        verify(console).println("Player 1 : 2");
        verify(console).println("Player 2 : 2");
        verify(console).println("Player 1 : 4");
        verify(console).println("Player 2 : 4");
    }

    @Test
    public void shouldPrintError() {
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        PrintStream console = mock(PrintStream.class);
        when(player1.move()).thenReturn(Move.UNKNOWN);
        when(player1.getBehaviour()).thenReturn(mock(ConsolePlayer.class) );
        when(player2.getBehaviour()).thenReturn(mock(ConsolePlayer.class) );
        Game game = new Game(player1, player2, machine, 1, console);

        game.play();

        verify(console).println("Found illegal input.");
    }

    @Test
    public void shouldGamePLayWellForBothPLayers(){
        Player player1 = new Player(new CooperateBehaviour(), "player1");
        Player player2 = new Player(new CheaterBehaviour(), "player1");
        Machine machine = new Machine();
        Game game = new Game(player1, player2, machine, 1, System.out);
        game.play();
        Assert.assertEquals(-1,player1.score());
        Assert.assertEquals(3,player2.score());
    }

    @Test
    public void shouldGamePLayWellForCopyCatPLayer(){
        Player player1 = new Player(new CheaterBehaviour(), "player1");
        Player player2 = new Player(new CopyCatBehaviour(), "player1");
        Machine machine = new Machine();
        Game game = new Game(player1, player2, machine, 3, System.out);
        game.play();
        Assert.assertEquals(3,player1.score());
        Assert.assertEquals(-1,player2.score());

        player1 = new Player(new CopyCatBehaviour(), "player1");
        player2 = new Player(new CheaterBehaviour(), "player1");
        game = new Game(player1, player2, machine, 3, System.out);
        game.play();
        Assert.assertEquals(-1,player1.score());
        Assert.assertEquals(3,player2.score());

        player1 = new Player(new CopyCatBehaviour(), "player1");
        player2 = new Player(new CopyCatBehaviour(), "player1");
        game = new Game(player1, player2, machine, 3, System.out);
        game.play();
        Assert.assertEquals(6,player1.score());
        Assert.assertEquals(6,player2.score());

    }
}
