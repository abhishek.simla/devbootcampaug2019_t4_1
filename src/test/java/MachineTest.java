import org.junit.Assert;
import org.junit.Test;

public class MachineTest {

    private Machine machine = new Machine();

    @Test
    public void shouldCreateMachineInstance() {
        Assert.assertNotNull(machine);
    }

    @Test
    public void shouldReturnScore() {
        Score score = machine.calculateScore(Move.COPERATE, Move.COPERATE);
        Assert.assertNotNull(score);
    }

    @Test
    public void shouldCalculateScoreWhenBothCoperate() {
        Score score = machine.calculateScore(Move.COPERATE, Move.COPERATE);
        Assert.assertEquals(2, score.getPlayer1Score());
        Assert.assertEquals(2, score.getPlayer2Score());
    }

    @Test
    public void shouldCalculateScoreWhenFirstCoperateAndSecondCheat() {
        Score score = machine.calculateScore(Move.COPERATE, Move.CHEAT);
        Assert.assertEquals(-1, score.getPlayer1Score());
        Assert.assertEquals(3, score.getPlayer2Score());
    }

    @Test
    public void shouldCalculateScoreWhenFirstCheatAndSecondCoperate() {
        Score score = machine.calculateScore(Move.CHEAT, Move.COPERATE);
        Assert.assertEquals(3, score.getPlayer1Score());
        Assert.assertEquals(-1, score.getPlayer2Score());
    }

    @Test
    public void shouldCalculateScoreWhenBothCheat() {
        Score score = machine.calculateScore(Move.CHEAT, Move.CHEAT);
        Assert.assertEquals(0, score.getPlayer1Score());
        Assert.assertEquals(0, score.getPlayer2Score());
    }


}
