import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PlayerTest {

    @Test
    public void shouldCreatePlayerInstance() {
        UserInput ui = new ConsoleInput(new Scanner(""));
        Player player = new Player(new ConsolePlayer(ui), "player1");
        assertNotNull(player);
    }

    @Test
    public void shouldReturnCoperate() {
        UserInput ui = new ConsoleInput(new Scanner("1"));
        Player player = new Player(new ConsolePlayer(ui), "player1");
        assertEquals(Move.COPERATE, player.move());
    }

    @Test
    public void shouldReturnCheat() {
        Scanner sc = new Scanner("2");
        UserInput ui = new ConsoleInput(new Scanner("0"));
        Player player = new Player(new ConsolePlayer(ui), "player1");
        assertEquals(Move.CHEAT, player.move());
    }

    @Test
    public void shouldReturnAlwaysCheat() {
        Player player = new Player(new CheaterBehaviour(), "player1");
        assertEquals(Move.CHEAT, player.move());
    }

    @Test
    public void shouldReturnAlwaysCooperate() {
        Player player = new Player(new CooperateBehaviour(), "player1");
        assertEquals(Move.COPERATE, player.move());
    }

    @Test
    public void shouldReturnUnknownMove() {
        Scanner sc = new Scanner("2");
        UserInput ui = new ConsoleInput(new Scanner("7"));
        Player player = new Player(new ConsolePlayer(ui), "player1");
        assertEquals(Move.UNKNOWN, player.move());
    }

    @Test
    public void shouldUpdateScoreFromZero() {
        UserInput ui = new ConsoleInput(new Scanner(""));
        Player player = new Player(new ConsolePlayer(ui), "player1");
        player.upateScore(2);
        assertEquals(2, player.score());
    }

    @Test
    public void shouldUpdateScoreFrom2() {
        UserInput ui = new ConsoleInput(new Scanner(""));
        Player player = new Player(new ConsolePlayer(ui), "player1");
        player.upateScore(2);
        player.upateScore(3);
        player.upateScore(-1);
        assertEquals(4, player.score());
    }

    @Test
    public void grudgerShouldCooperateByDefault() {
        GrudgerBehavior grudgerBehavior = mock(GrudgerBehavior.class);
        when(grudgerBehavior.move()).thenReturn(Move.COPERATE);
        Player grudgePlayer = new Player(grudgerBehavior, "player1");
        Assert.assertEquals(Move.COPERATE, grudgePlayer.move());
    }

    @Test
    public void playerShouldReturnName(){
        Player player1 = new Player(mock(CooperateBehaviour.class), "player1");
        Assert.assertEquals("player1", player1.name());
    }
}
