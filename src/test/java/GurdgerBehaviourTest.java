import org.junit.Assert;
import org.junit.Test;

import java.util.Observer;

import static org.mockito.Mockito.mock;

public class GurdgerBehaviourTest {

    @Test
    public void shouldGetInstance(){
        GrudgerBehavior grudgerBehavior = new GrudgerBehavior();
    }

    @Test
    public void shouldGetObserverInstance(){
        GrudgerBehavior grudgerBehavior = new GrudgerBehavior();
        Assert.assertTrue(grudgerBehavior instanceof Observer);
    }

    @Test
    public void shouldGiveCooperateByDefault(){
        GrudgerBehavior grudgerBehavior = new GrudgerBehavior();
        Assert.assertEquals(Move.COPERATE, grudgerBehavior.move());
    }

    @Test
    public void shouldAlwaysCheatOnceCheated(){
        GrudgerBehavior grudgerBehavior = new GrudgerBehavior();
        grudgerBehavior.update(mock(Game.class), Move.CHEAT);
        Assert.assertEquals(Move.CHEAT, grudgerBehavior.move());
    }

    @Test
    public void shouldKeepCheatingEvenIfOtherPlayerStartsCooperating(){
        GrudgerBehavior grudgerBehavior = new GrudgerBehavior();
        grudgerBehavior.update(mock(Game.class), Move.CHEAT);
        Assert.assertEquals(Move.CHEAT, grudgerBehavior.move());

        grudgerBehavior.update(mock(Game.class), Move.COPERATE);
        Assert.assertEquals(Move.CHEAT, grudgerBehavior.move());
    }

    @Test
    public void shouldInstantiateWithOpponentNameInput() {
        GrudgerBehavior grudgerBehavior = new GrudgerBehavior("sampleOpponentName");
        Assert.assertNotNull(grudgerBehavior);
    }
}
