import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class GameIntegrationTest {

    @Test
    public void gameShouldPlayWellWithGrudger(){
        Player player1 = new Player(new CopyCatBehaviour(), "player1");
        Player player2 = new Player(new GrudgerBehavior(), "player1");
        Machine machine = new Machine();
        Game game = new Game(player1, player2, machine, 5, System.out);
        game.play();
        Assert.assertEquals(10,player1.score());
        Assert.assertEquals(10,player2.score());

        player1 = new Player(new ConsolePlayer(new ConsoleInput(new Scanner("1\n1\n0\n1\n1"))), "player1");
        player2 = new Player(new GrudgerBehavior(), "player1");
        game = new Game(player1, player2, machine, 5, System.out);
        game.play();
        Assert.assertEquals(5,player1.score());
        Assert.assertEquals(9,player2.score());

    }

}
