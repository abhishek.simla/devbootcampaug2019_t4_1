public class Player {

    private PlayerBehaviour behaviour;
    private int score = 0;
    private final String name;

    public Player(PlayerBehaviour behaviour, String name) {
        this.behaviour = behaviour;
        this.name = name;
    }

    public Move move() {
        return this.behaviour.move();
    }

    public void upateScore(int roundScore) {
        this.score += roundScore;
    }

    public int score() {
        return this.score;
    }

    public PlayerBehaviour getBehaviour(){
        return this.behaviour;
    }
    
    public String name() {
        return name;
    }
}
