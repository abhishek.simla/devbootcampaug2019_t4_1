import java.util.Scanner;

public class ConsoleInput implements UserInput {
    public static final int COOP = 1;
    public static final int CHEAT = 0;
    Scanner sc;

    public ConsoleInput(Scanner sc) {
        this.sc = sc;
    }

    @Override
    public Move getMove() {
        int move = sc.nextInt();
        if (COOP == move) {
            return Move.COPERATE;
        } else if (CHEAT == move) {
            return Move.CHEAT;
        } return Move.UNKNOWN;
    }

}
