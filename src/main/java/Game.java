import java.io.PrintStream;
import java.util.Observable;

public class Game extends Observable {
    private final Player player1;
    private final Player player2;
    private final Machine machine;
    private final int noOfRounds;
    private final PrintStream console;

    public Game(Player player1, Player player2, Machine machine, int noOfRounds, PrintStream console) {

        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.noOfRounds = noOfRounds;
        this.console = console;
        if(this.player1.getBehaviour().needToNotified()){
            this.addObserver(this.player1.getBehaviour());
        }
        if(this.player2.getBehaviour().needToNotified()){
            this.addObserver(this.player2.getBehaviour());
        }

    }

    public void play() {
        for(int i = 0; i < noOfRounds; i++) {
            Move player1Move = player1.move();
            Move player2Move = player2.move();
            if(!player1.getBehaviour().needToNotified() && player2.getBehaviour().needToNotified()){
                this.setChanged();
                this.notifyObservers(player1Move);
            }
            if(player1.getBehaviour().needToNotified() && !player2.getBehaviour().needToNotified()){
                this.setChanged();
                this.notifyObservers(player2Move);
            }
            if(player1Move == Move.UNKNOWN || player2Move == Move.UNKNOWN){
                console.println("Found illegal input.");
                break;
            }
            Score score = machine.calculateScore(player1Move, player2Move);
            player1.upateScore(score.getPlayer1Score());
            player2.upateScore(score.getPlayer2Score());
            console.println(String.format("Player 1 : %d", player1.score()));
            console.println(String.format("Player 2 : %d", player2.score()));
        }
    }
}
