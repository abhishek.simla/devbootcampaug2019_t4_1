import java.util.Observable;

public class ConsolePlayer implements PlayerBehaviour{
    UserInput userInput;

    public ConsolePlayer( UserInput userInput){
        this.userInput = userInput;
    }

    @Override
    public boolean needToNotified() {
        return false;
    }

    @Override
    public Move move() {
        return userInput.getMove();
    }

    @Override
    public void update(Observable o, Object arg) {

    }
}