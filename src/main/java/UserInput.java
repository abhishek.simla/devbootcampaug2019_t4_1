@FunctionalInterface
public interface UserInput {
    Move getMove();
}
