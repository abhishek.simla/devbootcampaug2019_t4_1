import java.util.Observable;

public class CheaterBehaviour implements PlayerBehaviour {
        @Override
        public boolean needToNotified() {
            return false;
        }

        @Override
        public Move move() {
            return Move.CHEAT;
        }

        @Override
        public void update(Observable o, Object arg) {

    }
}
