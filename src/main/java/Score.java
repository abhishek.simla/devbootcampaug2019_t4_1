public class Score {

    private final int player1Score;
    private final int player2Score;

    Score(int player1Score, int player2Score){

        this.player1Score = player1Score;
        this.player2Score = player2Score;
    }

    public int getPlayer1Score() {
        return player1Score;
    }

    public int getPlayer2Score() {
        return player2Score;
    }
}
