import java.util.Observable;
import java.util.Observer;

public class CopyCatBehaviour implements PlayerBehaviour {

    private Move lastMove;

    @Override
    public boolean needToNotified() {
        return true;
    }

    @Override
    public Move move() {
        return lastMove == null ? Move.COPERATE : lastMove;
    }

    @Override
    public void update(Observable o, Object arg) {
        lastMove = (Move)arg;
    }
}
