import java.util.Observer;

public interface PlayerBehaviour extends Observer {
    public boolean needToNotified();
    public Move move();
}
