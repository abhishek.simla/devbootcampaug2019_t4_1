import java.util.Observable;

public class GrudgerBehavior implements PlayerBehaviour {
    private Move move = Move.COPERATE;
    private String opponentName = "";
    public GrudgerBehavior(){
    }

    public GrudgerBehavior(String opponentName){
        this.opponentName = opponentName;
    }

    @Override
    public boolean needToNotified() {
        return true;
    }

    @Override
    public Move move() {
        return this.move;
    }

    @Override
    public void update(Observable game, Object move) {
        if (move.equals(Move.CHEAT)){
            game.deleteObserver(this);
            this.move = (Move)move;
        }
    }
}
