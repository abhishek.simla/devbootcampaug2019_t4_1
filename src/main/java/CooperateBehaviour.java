import java.util.Observable;

public class CooperateBehaviour implements PlayerBehaviour {
    @Override
    public boolean needToNotified() {
        return false;
    }

    @Override
    public Move move() {
        return Move.COPERATE;
    }

    @Override
    public void update(Observable o, Object arg) {
    }
}
