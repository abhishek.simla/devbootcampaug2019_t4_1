public class Machine {

    public Score calculateScore(Move player1Move, Move player2Move) {
        if(Move.COPERATE == player1Move && player1Move == player2Move){
            return new Score(2, 2);
        }
        if(player1Move == Move.CHEAT && player2Move == Move.COPERATE){
            return new Score(3, -1);
        }
        if(player1Move == Move.COPERATE && player2Move == Move.CHEAT){
            return new Score(-1, 3);
        }
        return new Score(0, 0);
    }


}
